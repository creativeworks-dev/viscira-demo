import { AppRoutingModule, RouteComponents } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// -- Base Components

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NotFoundComponent } from './not-found/not-found.component';

// -- Inline Components

import { SelectionDetailAComponent } from './selection-detail-a/selection-detail-a.component';
import { SelectionDetailBComponent } from './selection-detail-b/selection-detail-b.component';
import { RegisterDetailAComponent } from './register-detail-a/register-detail-a.component';
import { RegisterDetailBComponent } from './register-detail-b/register-detail-b.component';

// -- Services

import { StateService } from './state.service';


@NgModule({
  declarations: [
    AppComponent,
    RouteComponents,
    ToolbarComponent,
    NotFoundComponent,
    SelectionDetailAComponent,
    SelectionDetailBComponent,
    RegisterDetailAComponent,
    RegisterDetailBComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [StateService],
  bootstrap: [AppComponent],
  entryComponents: [
    SelectionDetailAComponent,
    SelectionDetailBComponent,
    RegisterDetailAComponent,
    RegisterDetailBComponent
  ]
})
export class AppModule { }
