import { StateService } from './../state.service';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { SelectionDetailAComponent } from '../selection-detail-a/selection-detail-a.component';
import { SelectionDetailBComponent } from '../selection-detail-b/selection-detail-b.component';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss']
})
export class SelectionComponent implements OnInit {

  public dialog_is_visible: boolean = false;
  private detail_component = null;
  private user_flow = null;

  constructor(private state: StateService, private _cfr: ComponentFactoryResolver) { }

  ngOnInit() {

    this.state.base_state_emitter.subscribe(base_state => {
      this.user_flow = base_state.user_flow;
    });

    this.state.set_user_flow(null);
    this.state.set_navigation([
      {
        name: 'Home',
        link: '',
        active: true,
        current: false,
      },
      {
        name: 'Selection',
        link: '/selection',
        active: true,
        current: true,
      },
      {
        name: 'Register',
        link: '/register',
        active: false,
        current: false,
      },
      {
        name: 'Conclusion',
        link: '/conclusion',
        active: false,
        current: false,
      }
    ]);

  }

  @ViewChild("selection_detail", {read: ViewContainerRef, static: false}) container_detail: ViewContainerRef;

  selectionDetail(flow: string){

    this.dialog_is_visible = true;

    // --

    let child_component = null;

    if(flow === 'a'){
      child_component = this._cfr.resolveComponentFactory(SelectionDetailAComponent);
    } else if (flow === 'b') {
      child_component = this._cfr.resolveComponentFactory(SelectionDetailBComponent);
    } else {
      child_component = null;
    }

    // --

    this.detail_component ? this.detail_component.destroy() : null;
    this.detail_component = this.container_detail.createComponent(child_component);

    this.detail_component.instance.onCloseDialog.subscribe(() => {
      this.closeDialog();
    });

    this.detail_component.instance.onRegisterFlow.subscribe((flow: string) => {
      this.registerFlow(flow);
    });

  }

  closeDialog(){
    this.detail_component ? this.detail_component.destroy() : null;
    this.dialog_is_visible = false;
  }

  registerFlow(flow: string){
    this.state.set_user_flow(flow);
  }

}
