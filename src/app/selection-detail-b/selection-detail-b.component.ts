import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-selection-detail-b',
  templateUrl: './selection-detail-b.component.html',
  styleUrls: ['./selection-detail-b.component.scss']
})
export class SelectionDetailBComponent implements OnInit {

  @Output() onCloseDialog = new EventEmitter<any>();
  @Output() onRegisterFlow = new EventEmitter<string>();

  constructor(private router: Router) { }

  closeDialog(){
    this.onCloseDialog.emit();
  }

  registerFlow(){
    this.onRegisterFlow.emit('b');
    this.router.navigateByUrl('/register');
  }

  ngOnInit() {
  }

}
