import { IUserData } from './user_data';
import { INavigation } from './navigation';

export interface IBaseState {
  user_flow: any,
  user_data: IUserData,
  navigation: INavigation[]
}
