import { Injectable, Output, EventEmitter } from '@angular/core';
import { IUserData } from './interfaces/user_data';
import { INavigation } from './interfaces/navigation';
import { IBaseState } from './interfaces/base_state';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  base_state: IBaseState = {
    user_flow: null,
    user_data: {
      name: '',
      email: '',
      phone: ''
    },
    navigation: [
      {
        name: 'Home',
        link: '',
        active: false,
        current: false,
      },
      {
        name: 'Selection',
        link: '/selection',
        active: false,
        current: false,
      },
      {
        name: 'Register',
        link: '/register',
        active: false,
        current: false,
      },
      {
        name: 'Conclusion',
        link: '/conclusion',
        active: false,
        current: false,
      }
    ]
  };

  constructor() { }

  @Output() base_state_emitter = new EventEmitter<IBaseState>();

  get_state(){
    return this.base_state
  }

  set_user_flow(flow: string){
    this.base_state.user_flow = flow;
    this.base_state_emitter.emit(this.base_state)
  }

  set_user_data(user_data: IUserData){
    this.base_state.user_data = user_data;
    this.base_state_emitter.emit(this.base_state)
  }

  set_navigation(navigation: INavigation[]){
    this.base_state.navigation = navigation;
    this.base_state_emitter.emit(this.base_state)
  }

}
