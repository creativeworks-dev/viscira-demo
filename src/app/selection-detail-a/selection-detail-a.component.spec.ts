import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionDetailAComponent } from './selection-detail-a.component';

describe('SelectionDetailAComponent', () => {
  let component: SelectionDetailAComponent;
  let fixture: ComponentFixture<SelectionDetailAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectionDetailAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionDetailAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
