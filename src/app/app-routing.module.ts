import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntroComponent } from './intro/intro.component';
import { SelectionComponent } from './selection/selection.component';
import { RegisterComponent } from './register/register.component';
import { ConclusionComponent } from './conclusion/conclusion.component';
import { NotFoundComponent } from './not-found/not-found.component';


const routes: Routes = [
  {path: '', component: IntroComponent},
  {path: 'selection', component: SelectionComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'conclusion', component: ConclusionComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RouteComponents = [
  IntroComponent,
  SelectionComponent,
  RegisterComponent,
  ConclusionComponent,
]
