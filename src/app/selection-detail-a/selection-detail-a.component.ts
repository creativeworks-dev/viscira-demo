import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-selection-detail-a',
  templateUrl: './selection-detail-a.component.html',
  styleUrls: ['./selection-detail-a.component.scss']
})
export class SelectionDetailAComponent implements OnInit {

  @Output() onCloseDialog = new EventEmitter<any>();
  @Output() onRegisterFlow = new EventEmitter<string>();

  constructor(private router: Router) { }

  closeDialog(){
    this.onCloseDialog.emit();
  }

  registerFlow(){
    this.onRegisterFlow.emit('a');
    this.router.navigateByUrl('/register');
  }

  ngOnInit() {
  }

}
