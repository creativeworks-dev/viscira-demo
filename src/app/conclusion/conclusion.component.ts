import { StateService } from './../state.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conclusion',
  templateUrl: './conclusion.component.html',
  styleUrls: ['./conclusion.component.scss']
})
export class ConclusionComponent implements OnInit {

  conclusion_state = null;

  constructor(private state: StateService) { }

  ngOnInit() {

    this.state.base_state_emitter.subscribe(base_state => {
      this.conclusion_state = base_state;
    });

    this.state.set_navigation([
      {
        name: 'Home',
        link: '',
        active: true,
        current: false,
      },
      {
        name: 'Selection',
        link: '/selection',
        active: false,
        current: false,
      },
      {
        name: 'Register',
        link: '/register',
        active: false,
        current: false,
      },
      {
        name: 'Conclusion',
        link: '/conclusion',
        active: true,
        current: true,
      }
    ]);
  }

}
