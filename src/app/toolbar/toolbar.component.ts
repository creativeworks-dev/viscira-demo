import { StateService } from './../state.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  navigation_bar = [];

  constructor(private state: StateService) { }

  ngOnInit() {
    this.state.base_state_emitter.subscribe(base_state => {
      this.navigation_bar = base_state.navigation;
    });
  }

}
