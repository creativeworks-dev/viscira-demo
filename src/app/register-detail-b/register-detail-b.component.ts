import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IExternalUser } from '../interfaces/external_user';


@Component({
  selector: 'app-register-detail-b',
  templateUrl: './register-detail-b.component.html',
  styleUrls: ['./register-detail-b.component.scss']
})
export class RegisterDetailBComponent implements OnInit {

  external_url = 'http://jsonplaceholder.typicode.com/users';
  external_users = null;

  @Output() onChangeDisplay = new EventEmitter<string>();

  constructor(private http : HttpClient) { }

  ngOnInit() {

    this.http.get<IExternalUser[]>(this.external_url).subscribe(data => this.external_users = data)

  }

  changeDisplay(type: string){
    this.onChangeDisplay.emit(type);
  }

}
