import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-register-detail-a',
  templateUrl: './register-detail-a.component.html',
  styleUrls: ['./register-detail-a.component.scss']
})
export class RegisterDetailAComponent implements OnInit {

  @Output() onChangeDisplay = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  changeDisplay(type: string){
    this.onChangeDisplay.emit(type);
  }

}
