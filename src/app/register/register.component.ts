import { StateService } from '../state.service';

import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RegisterDetailAComponent } from './../register-detail-a/register-detail-a.component';
import { RegisterDetailBComponent } from './../register-detail-b/register-detail-b.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public dialog_is_visible: boolean = false;
  private detail_component = null;
  private user_flow = null;

  private formRegister = new FormGroup({
    fieldName: new FormControl(''),
    fieldEmail: new FormControl(''),
    fieldPhone: new FormControl(''),
  })

  constructor(private state: StateService, private router: Router, private _cfr: ComponentFactoryResolver) { }

  ngOnInit() {

    this.state.base_state_emitter.subscribe(base_state => {
      this.user_flow = base_state.user_flow;
    });

    this.state.set_navigation([
      {
        name: 'Home',
        link: '',
        active: true,
        current: false,
      },
      {
        name: 'Selection',
        link: '/selection',
        active: true,
        current: false,
      },
      {
        name: 'Register',
        link: '/register',
        active: true,
        current: true,
      },
      {
        name: 'Conclusion',
        link: '/conclusion',
        active: false,
        current: false,
      }
    ]);

  }

  @ViewChild("register_detail", {read: ViewContainerRef, static: false}) container_detail: ViewContainerRef;

  displayDetail(detail_type: string){

    this.dialog_is_visible = true;

    // --

    let child_component = null;

    if(detail_type === 'a'){
      child_component = this._cfr.resolveComponentFactory(RegisterDetailAComponent);
    } else if (detail_type === 'b') {
      child_component = this._cfr.resolveComponentFactory(RegisterDetailBComponent);
    } else {
      child_component = null;
    }

    // --

    this.detail_component ? this.detail_component.destroy() : null;
    this.detail_component = this.container_detail.createComponent(child_component);

    this.detail_component.instance.onChangeDisplay.subscribe((type: string) => {
      this.displayDetail(type);
    });

  }

  registerUserData(){

    let formRegisterValue = this.formRegister.value;

    this.state.set_user_data({
      name: formRegisterValue.fieldName,
      email: formRegisterValue.fieldEmail,
      phone: formRegisterValue.fieldPhone
    });

    if(this.user_flow == 'a'){
      this.displayDetail('a');
    } else if(this.user_flow == 'b'){
      this.router.navigateByUrl('/conclusion');
    } else {
      this.router.navigateByUrl('');
    }

  }

}
