import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterDetailAComponent } from './register-detail-a.component';

describe('RegisterDetailAComponent', () => {
  let component: RegisterDetailAComponent;
  let fixture: ComponentFixture<RegisterDetailAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterDetailAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterDetailAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
