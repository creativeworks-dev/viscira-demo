import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectionDetailBComponent } from './selection-detail-b.component';

describe('SelectionDetailBComponent', () => {
  let component: SelectionDetailBComponent;
  let fixture: ComponentFixture<SelectionDetailBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectionDetailBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionDetailBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
