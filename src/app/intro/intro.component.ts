import { StateService } from './../state.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  constructor(private state: StateService) { }

  ngOnInit() {

    this.state.set_user_flow(null);
    this.state.set_navigation([
      {
        name: 'Home',
        link: '',
        active: true,
        current: true,
      },
      {
        name: 'Selection',
        link: '/selection',
        active: false,
        current: false,
      },
      {
        name: 'Register',
        link: '/register',
        active: false,
        current: false,
      },
      {
        name: 'Conclusion',
        link: '/conclusion',
        active: false,
        current: false,
      }
    ]);

  }

}
