import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterDetailBComponent } from './register-detail-b.component';

describe('RegisterDetailBComponent', () => {
  let component: RegisterDetailBComponent;
  let fixture: ComponentFixture<RegisterDetailBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterDetailBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterDetailBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
